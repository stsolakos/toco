import sys
import argparse

import wx
import prettierfier

from bs4 import BeautifulSoup
from bs4 import NavigableString
from bs4 import Tag


parser = argparse.ArgumentParser()
parser.add_argument("-b", "--base-id", help="Sets the base id.", type=int, action="store", default=12000)
parser.add_argument("-d", "--default-only", help="Sets the base id.", action="store_true", default=False)
parser.add_argument("input", help="The input file.", action="store")
args = parser.parse_args()


BASE_ID = args.base_id
REPLACE_DEFAULT_ONLY = args.default_only
SOURCE_FILE = args.input

#BeautifulStoneSoup.NESTABLE_TAGS["document"] = ["document"]

f = open(SOURCE_FILE, "rb")
xml = f.read()
f.close()

soup = BeautifulSoup(xml, "lxml-xml")
all_documents = soup.findAll("document", recursive=True)
current_id = BASE_ID

c = 0
for tag in all_documents:
    the_name = tag.find("string", attrs={"name": "proxy-Id name"}, recursive=False)
    the_id = tag.find("long", attrs={"name": "proxy-Id value"}, recursive=False)

    try:
        name = the_name.string[1:-1]
        id = int(the_id.string)
    except AttributeError:
        continue
    except TypeError:
        continue
    except:
        raise

    if name[:2] == "wx":
        new_id = eval(name[:2] + "." + name[2:])

    elif REPLACE_DEFAULT_ONLY:
        if id == -1:
            new_id = current_id
            current_id += 1
        else:
            new_id = id

    else:
        new_id = current_id
        current_id += 1

    new_tag = soup.new_tag(name="long")
    new_tag["name"] = "proxy-Id value"
    new_tag.insert(0, NavigableString(str(new_id)))

    the_id.replaceWith(new_tag)

print(prettierfier.prettify_xml(str(soup), indent=2))
