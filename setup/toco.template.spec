# -*- mode: python -*-

block_cipher = None



a = Analysis(["../../src/toco.py"],
             pathex=["../../setup/build/{ARCH_STR}/", "../lib"],
             binaries=None,
             datas=None,
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None,
             excludes=None,
             win_no_prefer_redirects=None,
             win_private_assemblies=None,
             cipher=block_cipher)

a.datas += [("toco.ico", "../../rsrc/art/icon/toco.ico", "DATA")]



pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)



exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name="toco",
          debug={DEBUG},
          strip=None,
          upx=False,
          console={CONSOLE},
          icon=r"{TOCO_ICO}")



coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=False,
               name="toco")
