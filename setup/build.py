import sys
import os
import os.path
import subprocess
import time
import argparse
import shutil
import datetime

sys.path.insert(1, '../lib')
sys.path.insert(1, '../src')

from common.tocenv import TocoEnv



ENV = TocoEnv.Get()


parser = argparse.ArgumentParser()
parser.add_argument('-c', '--console', help='Sets whether the resulting binary will run opening a python console.', action='store_true', default=False)
parser.add_argument('-d', '--debug', help='Sets whether the resulting binary will display debug messages.', action='store_true', default=False)
parser.add_argument('-l', '--clean-up', help='Clean up intermediate files when done.', action='store_true', default=False)
parser.add_argument('-s', '--setup-package', help='Build setup package.', action='store_true', default=False)
args = parser.parse_args()

if ENV.Runtime.IsWindows:
    TARGET = 'win'
else:
    TARGET = 'linux'

ARCH                = int(ENV.Runtime.Arch)
ARCH_STR            = '{0}_{1}'.format(TARGET, {32: 'x86', 64: 'amd64'}[ARCH])

BUILD_SETUP_PACKAGE = args.setup_package
CLEANUP_WHEN_DONE   = args.clean_up
CONSOLE             = args.console
DEBUG               = args.debug



def PATH_TOP(*args):
    return os.path.realpath(ENV.Paths.TopLevel(*args))


def PATH_RSRC(*args):
    return os.path.realpath(os.path.join(PATH_TOP('rsrc'), *args))


def PATH_SRC(*args):
    return os.path.realpath(os.path.join(PATH_TOP('src'), *args))


def PATH_SETUP(*args):
    return os.path.realpath(os.path.join(PATH_TOP('setup'), *args))


def PATH_DIST(*args):
    return os.path.realpath(os.path.join(PATH_SETUP(), ARCH_STR, 'dist', *args))


def PATH_PACK(*args):
    return os.path.realpath(os.path.join(PATH_SETUP(), ARCH_STR, 'package', *args))


# 0. Find the build number.
#TODO: Find the revision number from git
revision = time.strftime('%Y%m%d')



# 1. Create the resource modules.
f = open(PATH_RSRC('toco.xrc'), 'rt')
res = f.read()
res = res.replace('\r', '')
f.close()

f = open(PATH_SRC('gui', 'setup_resources.py'), 'wt')
f.write('TOCO_RESOURCE_STR = \'\'\'')
f.write(res)
f.write('\'\'\'\n')
f.write('\n')
f.write('TOCO_REVISION = \'%s\'\n' % revision)
f.close()



# 2. Build and copy the executable.
stropt_debug = '--debug=all' if DEBUG else ''

shutil.rmtree(PATH_DIST(), ignore_errors=True)

if TARGET.startswith('win'):
    PY_INSTALLER = 'python -m PyInstaller'.format(sys.version_info[0], ARCH)
elif TARGET.startswith('linux'):
    PY_INSTALLER = 'python{0} -m PyInstaller'.format(sys.version_info[0])

try:
    os.makedirs(PATH_SETUP(ARCH_STR))
except:
    pass

f = open(PATH_SETUP('toco.template.spec'), 'rt')
spec_data = f.read()
f.close()

spec_data = spec_data.replace('{DEBUG}', str(DEBUG))
spec_data = spec_data.replace('{CONSOLE}', str(CONSOLE))
spec_data = spec_data.replace('{ARCH_STR}', ARCH_STR)
spec_data = spec_data.replace('{TOCO_ICO}', PATH_RSRC('art', 'icon', 'toco.ico'))

f = open(PATH_SETUP(ARCH_STR, 'toco.spec'), 'wt')
f.write(spec_data)
f.close()

cmd_str = '{py_installer} --distpath={path_dist} --workpath={path_work} -y {spec_file} {opt_debug}'.format(py_installer=PY_INSTALLER, path_dist=PATH_DIST(), path_work=os.path.join(ARCH_STR, 'build'), spec_file=os.path.join(ARCH_STR, 'toco.spec'),  opt_debug=stropt_debug)
sys.stderr.write('{0}\n'.format(cmd_str))

subprocess.Popen(cmd_str, shell=True).wait()


# 3. Copy art.
for artdir in ('icon', 'png'):
    fulldir = PATH_DIST('toco', 'art', artdir)
    if not os.path.isdir(fulldir):
        os.makedirs(fulldir, 0o755)

for artfile in (
    ('icon', 'toco.ico'),
    ):  shutil.copyfile(PATH_RSRC('art', *artfile), PATH_DIST('toco', 'art', *artfile))

if ENV.Runtime.IsWindows:
    cp_rec = 'xcopy /E /Y'
else:
    cp_rec = 'cp -fr'


# 4. Create the setup package.
if BUILD_SETUP_PACKAGE:
    if sys.platform.startswith('win'):
        try:
            import _winreg as winreg
        except ImportError:
            import winreg

        f = open(PATH_SETUP('toco.template.iss'), 'rt')
        iss_data = f.read()
        f.close()

        iss_data = iss_data.replace('{ARCH_STR}', ARCH_STR)
        #iss_data = iss_data.replace('{RELEASE_STR}', CFG.ReleaseStr)
        iss_script_filename = PATH_SETUP(ARCH_STR, 'toco.{0}.iss'.format(ARCH_STR))

        f = open(iss_script_filename, 'wt')
        f.write(iss_data)
        f.close()

        h = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)
        cmd_str = winreg.QueryValue(h, r'SOFTWARE\Classes\InnoSetupScriptFile\shell\open\command')
        cmd_str = cmd_str.replace('%1', iss_script_filename)
        cmd_str = cmd_str.replace('Compil32', 'iscc')
        cmd_str = cmd_str.strip()
        winreg.CloseKey(h)

        subprocess.Popen(cmd_str, shell=True).wait()


# 5. Cleanup.
if CLEANUP_WHEN_DONE:
    for fname in (
        PATH_SRC('gui', 'setup_resources.py'),
        PATH_SRC('gui', 'setup_resources.pyc'),
    ):
        try:
            os.remove(fname)
        except Exception as e:
            pass
