import os
import os.path
import ctypes

try:
    from ctypes import wintypes
except:
    # Must not be Windows...
    pass



class TocoEnv(object):
    TOCO_ENV = None

    def __init__(self):
        # We avoid importing from the top of the file because this singleton is
        # used from the partial environment component modules. This is an emulation
        # of 'lazy importing'.
        from .files import TocoFiles
        from .paths import TocoPaths
        from .runtime import TocoRuntime
        from .tools import TocoTools
        self._Files = TocoFiles()
        self._Paths = TocoPaths()
        self._Runtime = TocoRuntime()
        self._Tools = TocoTools()
        return


    @staticmethod
    def ShortPath(long_name):
        if not TocoEnv.Get().Runtime.IsWindows:
            return long_name

        _GetShortPathNameW = ctypes.windll.kernel32.GetShortPathNameW
        _GetShortPathNameW.argtypes = [wintypes.LPCWSTR, wintypes.LPWSTR, wintypes.DWORD]
        _GetShortPathNameW.restype = wintypes.DWORD

        output_buf_size = 0
        while True:
            output_buf = ctypes.create_unicode_buffer(output_buf_size)
            needed = _GetShortPathNameW(long_name, output_buf, output_buf_size)

            if output_buf_size >= needed:
                if not len(output_buf.value):
                    raise ValueError("Could not convert {0} to short format.".format(long_name))

                return output_buf.value
            else:
                output_buf_size = needed

        return None


    @classmethod
    def Get(cls):
        if TocoEnv.TOCO_ENV is None:
            TocoEnv.TOCO_ENV = TocoEnv()

        return TocoEnv.TOCO_ENV


    def Dump(self):
        ret = ""
        ret += "Files\n"
        for ln in [sl.rstrip() for sl in self.Files.Dump().split("\n")]:
            ret += "    {0}\n".format(ln)
        ret += "Paths\n"
        for ln in [sl.rstrip() for sl in self.Paths.Dump().split("\n")]:
            ret += "    {0}\n".format(ln)
        ret += "Runtime\n"
        for ln in [sl.rstrip() for sl in self.Runtime.Dump().split("\n")]:
            ret += "    {0}\n".format(ln)
        ret += "Tools\n"
        for ln in [sl.rstrip() for sl in self.Tools.Dump().split("\n")]:
            ret += "    {0}\n".format(ln)
        return ret


    @property
    def Files(self):
        return self._Files


    @property
    def Paths(self):
        return self._Paths


    @property
    def Runtime(self):
        return self._Runtime


    @property
    def Tools(self):
        return self._Tools
