import os
import os.path
import sys

import winpath

from . import TocoEnv



class TocoPaths(object):
    def __init__(self):
        self._AppData = None
        self._CommonAppData = None
        self._ExecBase = None
        self._TopLevel = None
        self._TocoLogBase = None
        self._TocoConfigBase = None
        self._TocoTempBase = None
        return


    def Dump(self):
        ret = ""
        ret += "{0:40} : {1}\n".format("TocoPaths.Appdata", self.AppData())
        ret += "{0:40} : {1}\n".format("TocoPaths.CommonAppdata", self.CommonAppData())
        ret += "{0:40} : {1}\n".format("TocoPaths.ExecBase", self.ExecBase())
        ret += "{0:40} : {1}\n".format("TocoPaths.TopLevel", self.TopLevel())
        ret += "{0:40} : {1}\n".format("TocoPaths.TocoLogBase", self.TocoLogBase())
        ret += "{0:40} : {1}\n".format("TocoPaths.TocoConfigBase", self.TocoConfigBase())
        ret += "{0:40} : {1}\n".format("TocoPaths.TocoInstallerBase", self.TocoInstallerBase())
        ret += "{0:40} : {1}\n".format("TocoPaths.TocoInstallerConfigBase", self.TocoInstallerConfigBase())
        ret += "{0:40} : {1}\n".format("TocoPaths.TocoTempBase", self.TocoTempBase())
        return ret


    def AppData(self, *args):
        if self._AppData is None:
            e = TocoEnv.Get()
            if e.Runtime.IsWindows:
                self._AppData = os.path.realpath(os.path.join(winpath.get_local_appdata(), "toco"))
            elif e.Runtime.IsLinux:
                self._AppData = os.path.realpath(os.path.join("~", ".toco"))

            try:
                os.makedirs(self._AppData)
            except:
                pass

        return os.path.realpath(os.path.join(self._AppData, *args))


    def CommonAppData(self, *args):
        if self._CommonAppData is None:
            e = TocoEnv.Get()
            if e.Runtime.IsWindows:
                self._CommonAppData = os.path.realpath(os.path.join(winpath.get_common_appdata(), "toco"))
            elif e.Runtime.IsLinux:
                self._CommonAppData = os.path.realpath(os.path.join("/", "opt", "toco"))

            try:
                os.makedirs(self._CommonAppData)
            except:
                pass

        return os.path.realpath(os.path.join(self._CommonAppData, *args))


    def ExecBase(self, *args):
        if self._ExecBase is None:
            e = TocoEnv.Get()

            # If the application is run as a bundle, the pyInstaller bootloader
            # extends the sys module by a flag frozen=True and sets the app
            # path into variable _MEIPASS'.
            if e.Runtime.IsBinary:
                self._ExecBase = sys._MEIPASS
            else:
                self._ExecBase = os.path.realpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", ".."))

            try:
                os.makedirs(self._ExecBase)
            except:
                pass

        return os.path.realpath(os.path.join(self._ExecBase, *args))


    def TopLevel(self, *args):
        if self._TopLevel is None:
            e = TocoEnv.Get()

            if e.Runtime.IsBinary:
                self._TopLevel = self._ExecBase
            else:
                self._TopLevel = None
                checked = []
                current_path = os.path.abspath(__file__)

                while 1:
                    to_check = os.path.join(current_path, ".toplevelmarker")

                    if to_check in checked:
                        # Checked everything, no need to check anything else.
                        break

                    if os.path.isfile(to_check):
                        self._TopLevel = current_path
                        break

                    checked.append(to_check)
                    current_path, filename = os.path.split(current_path)

        try:
            os.makedirs(self._TopLevel)
        except:
            pass

        return os.path.realpath(os.path.join(self._TopLevel, *args))


    def TocoLogBase(self, *args):
        if self._TocoLogBase is None:
            e = TocoEnv.Get()
            if e.Runtime.IsWindows:
                self._TocoLogBase = self.AppData("logs")
            elif e.Runtime.IsLinux:
                self._TocoLogBase = os.path.join("/", "opt", "toco", "var", "log")

            try:
                os.makedirs(self._TocoLogBase)
            except:
                pass

        return os.path.realpath(os.path.join(self._TocoLogBase, *args))


    def TocoConfigBase(self, *args):
        if self._TocoConfigBase is None:
            self._TocoConfigBase = self.AppData("config")

            try:
                os.makedirs(self._TocoConfigBase)
            except:
                pass

        return os.path.realpath(os.path.join(self._TocoConfigBase, *args))


    def TocoTempBase(self, *args):
        if self._TocoTempBase is None:
            self._TocoTempBase = self.AppData("tmp")

            try:
                os.makedirs(self._TocoTempBase)
            except:
                pass

        return os.path.realpath(os.path.join(self._TocoTempBase, *args))
