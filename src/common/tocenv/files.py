import os
import os.path
import datetime

from . import TocoEnv



class TocoFiles(object):
    def __init__(self):
        self._TocoLogErr = None
        self._TocoLogFull = None
        self._TocoVersion = None
        self._TocoConfig = None
        return


    def Dump(self):
        ret = ""
        ret += "{0:40} : {1}\n".format("TocoFiles.TocoLogErr", self.TocoLogErr)
        ret += "{0:40} : {1}\n".format("TocoFiles.TocoLogFull", self.TocoLogFull)
        ret += "{0:40} : {1}\n".format("TocoFiles.TocoVersion", self.TocoVersion)
        ret += "{0:40} : {1}\n".format("TocoFiles.TocoConfig", self.TocoConfig)
        return ret


    @property
    def TocoLogErr(self):
        if self._TocoLogErr is None:
            e = TocoEnv.Get()
            self._TocoLogErr = e.Paths.TocoLogBase("toco.err.log")

        return self._TocoLogErr


    @property
    def TocoLogFull(self):
        if self._TocoLogFull is None:
            e = TocoEnv.Get()
            self._TocoLogFull = e.Paths.TocoLogBase("toco.full.log")

        return self._TocoLogFull


    @property
    def TocoVersion(self):
        if self._TocoVersion is None:
            e = TocoEnv.Get()
            self._TocoVersion = e.Paths.TocoConfigBase("toco.version.xml")

        return self._TocoVersion


    @property
    def TocoConfig(self):
        if self._TocoConfig is None:
            e = TocoEnv.Get()
            self._TocoConfig = e.Paths.TocoConfigBase("toco.conf.xml")

        return self._TocoConfig
