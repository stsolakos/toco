import sys
import os
import os.path

from . import TocoEnv


class TocoRuntime(object):
    def __init__(self):
        self._Binary = None
        self._Platform = None
        self._Arch = None
        return


    def Dump(self):
        ret = ""
        ret += "{0:30} : {1}\n".format("TocoRuntime.IsBinary", self.IsBinary)
        ret += "{0:30} : {1}\n".format("TocoRuntime.IsSource", self.IsSource)
        ret += "{0:30} : {1}\n".format("TocoRuntime.IsWindows", self.IsWindows)
        ret += "{0:30} : {1}\n".format("TocoRuntime.IsLinux", self.IsLinux)
        ret += "{0:30} : {1}\n".format("TocoRuntime.Platform", self.Platform)
        ret += "{0:30} : {1}\n".format("TocoRuntime.Arch", self.Arch)
        return ret


    @property
    def IsBinary(self):
        if self._Binary is None:
            self._Binary = getattr(sys, "frozen", False)

        return self._Binary


    @property
    def IsSource(self):
        return not self.IsBinary


    @property
    def Platform(self):
        if self._Platform is None:
            if sys.platform.startswith("win"):
                self._Platform = "windows"
            else:
                self._Platform = "linux"

        return self._Platform


    @property
    def Arch(self):
        if self._Arch is None:
            if sys.maxsize > 2 ** 32:
                self._Arch = 64
            else:
                self._Arch = 32

        return self._Arch


    @property
    def IsWindows(self):
        return (self.Platform == "windows")


    @property
    def IsLinux(self):
        return (self.Platform == "linux")
