import logging
import uuid

from .xmlbase import XmlConfig


LOG = logging.getLogger("")



class AgentAppConfig(XmlConfig):
    def __init__(self):
        XmlConfig.__init__(self)
        self._GUID = None
        self._Master = None
        self._Name = None
        self._CompanyId = None
        self._CompanyName = None
        self._VesselId = None
        self._VesselName = None
        self._Debug = None
        self._ManualScheduling = None
        self._AutoStart = {"first_run": [], "startup": []}
        self._TaskXmlTags = {}
        return


    def Dump(self):
        ret = ""
        ret += "{0:30} : {1}\n".format("self.GUID", self.GUID)
        ret += "{0:30} : {1}\n".format("self.Master", self.Master)
        ret += "{0:30} : {1}\n".format("self.Name", self.Name)
        ret += "{0:30} : {1}\n".format("self.CompanyId", self.CompanyId)
        ret += "{0:30} : {1}\n".format("self.CompanyName", self.CompanyName)
        ret += "{0:30} : {1}\n".format("self.VesselId", self.VesselId)
        ret += "{0:30} : {1}\n".format("self.VesselName", self.VesselName)
        ret += "{0:30} : {1}\n".format("self.Debug", self.Debug)
        ret += "{0:30} : {1}\n".format("self.ManualScheduling", self.ManualScheduling)
        ret += "{0:30} : {1}\n".format("self.TaskXmlTags", self.TaskXmlTags)
        ret += "{0:30} : {1}\n".format("self.AutoFirstRun", self.AutoFirstRun)
        ret += "{0:30} : {1}\n".format("self.AutoStartup", self.AutoStartup)
        return ret


    @property
    def GUID(self):
        return self._GUID


    @GUID.setter
    def GUID(self, value):
        self._GUID = uuid.UUID(value)
        return


    @property
    def Master(self):
        return self._Master


    @Master.setter
    def Master(self, value):
        self._Master = value
        return


    @property
    def Name(self):
        return self._Name


    @Name.setter
    def Name(self, value):
        self._Name = value
        return


    @property
    def CompanyId(self):
        return self._CompanyId


    @CompanyId.setter
    def CompanyId(self, value):
        self._CompanyId = int(value)
        return


    @property
    def CompanyName(self):
        return self._CompanyName


    @CompanyName.setter
    def CompanyName(self, value):
        self._CompanyName = value
        return


    @property
    def VesselId(self):
        return self._VesselId


    @VesselId.setter
    def VesselId(self, value):
        self._VesselId = int(value)
        return


    @property
    def VesselName(self):
        return self._VesselName


    @VesselName.setter
    def VesselName(self, value):
        self._VesselName = value
        return


    @property
    def Debug(self):
        return self._Debug


    @Debug.setter
    def Debug(self, value):
        self._Debug = int(value)
        return


    @property
    def ManualScheduling(self):
        return self._ManualScheduling


    @ManualScheduling.setter
    def ManualScheduling(self, value):
        self._ManualScheduling = int(value)
        return


    @property
    def AutoFirstRun(self):
        return self._AutoStart["first_run"]


    @property
    def AutoStartup(self):
        return self._AutoStart["startup"]


    @property
    def TaskXmlTags(self):
        return self._TaskXmlTags


    def LoadFile(self, filename):
        with open(filename, "rb") as f:
            self.Load(f.read().decode("utf8"))
            f.close()

        return


    def Load(self, data):
        XmlConfig.Load(self, data)

        # Read agent configuration
        tag_agent = self.ReadTag(self.Root, "agent", optional=False)

        self.GUID = self.ReadTagText(tag_agent, "GUID")
        self.Name = self.ReadTagText(tag_agent, "name")
        self.Master = self.ReadTagText(tag_agent, "master")
        self.CompanyId = self.ReadTagText(tag_agent, "company_id")
        self.CompanyName = self.ReadTagText(tag_agent, "company_name")
        self.VesselId = self.ReadTagText(tag_agent, "vessel_id")
        self.VesselName = self.ReadTagText(tag_agent, "vessel_name")
        self.Debug = self.ReadTagText(tag_agent, "debug", default="0")
        self.ManualScheduling = self.ReadTagText(tag_agent, "manual_scheduling", default="0")

        tag_tasks = self.ReadTag(tag_agent, "tasks", optional=True)

        if tag_tasks is None:
            LOG.warning("No tasks were found in the configuration file.")
            return

        for tag_task in self.ReadTags(tag_tasks, "task"):
            name = self.ReadTagText(tag_task, "name")
            self.TaskXmlTags[name] = tag_task

        tag_autostart = self.ReadTag(tag_agent, "autostart", optional=True)

        if tag_autostart is None:
            LOG.warning("No autostart information was found in the configuration file.")
            return

        tag_first_run = self.ReadTag(tag_autostart, "first_run", optional=True)

        for tag_task in self.ReadTags(tag_first_run, "task"):
            self.AutoFirstRun.append(tag_task.text)

        tag_startup = self.ReadTag(tag_autostart, "startup", optional=True)

        for tag_task in self.ReadTags(tag_startup, "task"):
            self.AutoStartup.append(tag_task.text)

        return
