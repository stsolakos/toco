import logging
import datetime

try:
    from functools import reduce
except:
    pass

from .xmlbase import XmlConfig


LOG = logging.getLogger("")



class VersionConfig(XmlConfig):
    def __init__(self):
        XmlConfig.__init__(self)
        self._ReleaseStr = None
        self._BuildTime = None
        self._DigestCommit = None
        self._DigestInstaller = None
        self._DigestConfig = None
        return


    def Dump(self):
        ret = ""
        ret += "{0:30} : {1}\n".format("self.ReleaseStr", self.ReleaseStr)
        ret += "{0:30} : {1}\n".format("self.ReleaseTuple", self.ReleaseTuple)
        ret += "{0:30} : {1}\n".format("self.ReleaseInt", self.ReleaseInt)
        ret += "{0:30} : {1}\n".format("self.BuildTime", self.BuildTime)
        ret += "{0:30} : {1}\n".format("self.BuildTimeStr", self.BuildTimeStr)
        ret += "{0:30} : {1}\n".format("self.BuildTimeInt", self.BuildTimeInt)
        ret += "{0:30} : {1}\n".format("self.DigestCommit", self.DigestCommit)
        ret += "{0:30} : {1}\n".format("self.DigestInstaller", self.DigestInstaller)
        ret += "{0:30} : {1}\n".format("self.DigestConfig", self.DigestConfig)
        return ret


    def DumpXml(self):
        ret = ""
        ret += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        ret += "<version>\n"
        ret += "    <release>{}</release>\n".format(self.ReleaseStr if self.ReleaseStr is not None else "")
        ret += "    <build_time>{}</build_time>\n".format(self.BuildTimeStr if self.BuildTime is not None else "")
        ret += "\n"
        ret += "    <digest>\n"
        ret += "        <!-- The commit id of the head of the built branch (to be filled-in by git during checkout) -->\n"
        ret += "        <commit>{}</commit>\n".format(self.DigestCommit if self.DigestCommit is not None else "")
        ret += "\n"
        ret += "        <!-- The md5 sum digest of the installer's .exe package (to be filled-in by the build script) -->\n"
        ret += "        <installer>{}</installer>\n".format(self.DigestInstaller if self.DigestInstaller is not None else "")
        ret += "\n"
        ret += "        <!-- The md5 sum digest of the configuration file (to be filled-in by the service so as to restart when the file is modified by rsync) -->\n"
        ret += "        <config>{}</config>\n".format(self.DigestConfig if self.DigestConfig is not None else "")
        ret += "    </digest>\n"
        ret += "</version>"
        return ret


    @property
    def ReleaseStr(self):
        return self._ReleaseStr


    @ReleaseStr.setter
    def ReleaseStr(self, value):
        self._ReleaseStr = value
        return


    @property
    def ReleaseTuple(self):
        try:
            ret = tuple([int(f) for f in self.ReleaseStr.strip().split(".")])
        except:
            ret = (0, 0, 0,)

        return ret


    @property
    def ReleaseInt(self):
        return reduce(lambda x, y: x + y, [x[1] * (10 ** (6 - (3 * x[0]))) for x in enumerate(self.ReleaseTuple)])


    @property
    def BuildTime(self):
        return self._BuildTime


    @BuildTime.setter
    def BuildTime(self, value):
        try:
            if isinstance(value, datetime.datetime):
                self._BuildTime = value
            else:
                self._BuildTime = datetime.datetime.strptime(str(value), "%Y%m%d%H%M%S")
        except:
            LOG.exception("Unable to parse build time value: {}\n".format(value))
            self._BuildTime = None

        return


    @property
    def BuildTimeStr(self):
        if self.BuildTime is None:
            return ""

        return self._BuildTime.strftime("%Y%m%d%H%M%S")


    @property
    def BuildTimeInt(self):
        if self.BuildTime is None:
            return 0

        return int(self.BuildTimeStr)


    @property
    def DigestCommit(self):
        return self._DigestCommit


    @DigestCommit.setter
    def DigestCommit(self, value):
        self._DigestCommit = value
        return


    @property
    def DigestInstaller(self):
        return self._DigestInstaller


    @DigestInstaller.setter
    def DigestInstaller(self, value):
        self._DigestInstaller = value
        return


    @property
    def DigestConfig(self):
        return self._DigestConfig


    @DigestConfig.setter
    def DigestConfig(self, value):
        self._DigestConfig = value
        return


    def LoadFile(self, filename):
        with open(filename, "rb") as f:
            self.Load(f.read().decode("utf8"))
            f.close()

        return


    def Load(self, data):
        XmlConfig.Load(self, data)

        # Read version information
        self.ReleaseStr = self.ReadTagText(self.Root, "release")
        self.BuildTime = self.ReadTagText(self.Root, "build_time", default="0")

        tag_digest = self.ReadTag(self.Root, "digest", optional=False)
        self.DigestCommit = self.ReadTagText(tag_digest, "commit")
        self.DigestInstaller = self.ReadTagText(tag_digest, "installer")
        self.DigestConfig = self.ReadTagText(tag_digest, "config")

        return

