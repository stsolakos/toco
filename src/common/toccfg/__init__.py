import logging
import zipfile
import io

from common.agtenv import AgentEnv
from common.agtcrypt import AgentCrypt

from common.comix.terminals.config import TermConfig

from .xmlbase import XmlConfig
from .app import AgentAppConfig


LOG = logging.getLogger("")



# The agent's configuration file is actually a bunch of meaningless crypted
# data which when decrypted transform to a ZIP file containing these files:
#
# terminals.cfg
# agentapp.conf.xml
# id_rsa
#
class AgentConfigBundle(object):
    def __init__(self):
        self._Terminals = None
        self._Key = None
        self._App = None
        return


    @property
    def Terminals(self):
        return self._Terminals


    @Terminals.setter
    def Terminals(self, data):
        self._Terminals = bytes(data)
        return


    @property
    def Key(self):
        return self._Key


    @Key.setter
    def Key(self, data):
        self._Key = bytes(data)
        return


    @property
    def App(self):
        return self._App


    @App.setter
    def App(self, data):
        self._App = bytes(data)
        return


    def Load(self, filename):
        with open(filename, "rb") as zf:
            self.LoadData(zf.read())
            zf.close()

        return


    def Save(self, filename):
        with open(filename, "wb") as zf:
            zf.write(self.SaveData())
            zf.close()

        return


    def LoadData(self, data):
        a = AgentCrypt()
        a.Crypted = data

        zip_fp = io.BytesIO(a.Plain)
        zip_fp.seek(0)

        with zipfile.ZipFile(zip_fp, "r") as zf:
            self.Terminals = zf.read("terminals.cfg")
            self.Key = zf.read("id_rsa")
            self.App = zf.read("agentapp.conf.xml")
            zf.close()
            zip_fp.close()

        return


    def SaveData(self):
        a = AgentCrypt()
        zip_fp = io.BytesIO()

        with zipfile.ZipFile(zip_fp, "w") as zf:
            zf.writestr("terminals.cfg", self.Terminals, zipfile.ZIP_DEFLATED)
            zf.writestr("id_rsa", self.Key, zipfile.ZIP_DEFLATED)
            zf.writestr("agentapp.conf.xml", self.App, zipfile.ZIP_DEFLATED)
            zf.close()

            a.Plain = zip_fp.getvalue()
            zip_fp.close()

        return a.Crypted



class AgentConfig(object):
    AGENT_CFG = None

    def __init__(self):
        self._Bundle = AgentConfigBundle()
        self._CfgTerminals = TermConfig()
        self._CfgApp = AgentAppConfig()
        self._Key = None
        self._Ok = False
        return


    @classmethod
    def Get(cls):
        if AgentConfig.AGENT_CFG is None:
            AgentConfig.AGENT_CFG = AgentConfig()

            e = AgentEnv.Get()

            if e.Runtime.IsOnComix:
                AgentConfig.AGENT_CFG.LoadFileSystem()
            else:
                AgentConfig.AGENT_CFG.LoadFile(AgentEnv.Get().Paths.AgentConfigBase("agent.config.acz"))

        return AgentConfig.AGENT_CFG


    def LoadFileSystem(self):
        e = AgentEnv.Get()

        try:
            with open(e.Paths.ComixConfig("terminals.cfg"), "rb") as f:
                self._CfgTerminals.Load(f.read().decode("utf8"))
                f.close()

            with open(e.Paths.ComixConfig("agentapp.conf.xml"), "rb") as f:
                self._CfgApp.Load(f.read().decode("utf8"))
                f.close()

            with open("/home/comix/id_rsa", "rb") as f:
                self._Key = f.read()
                f.close()

            self._Ok = True
        except:
            LOG.exception("Exception while loading configuration.")
            self._Ok = False
        return


    def LoadFile(self, filename):
        try:
            self._Bundle.Load(filename)
            self._CfgTerminals.Load(self._Bundle.Terminals.decode("utf8"))
            self._CfgApp.Load(self._Bundle.App.decode("utf8"))
            self._Key = self._Bundle.Key
            self._Ok = True
        except:
            LOG.exception("Exception while loading configuration.")
            self._Ok = False
        return


    def Dump(self):
        ret = ""

        ret += "Terminals\n"
        ret += "-------------------------------------------------\n"

        for tl in [ln.strip() for ln in self.Terminals.Dump().split("\n")]:
            ret += "    {0}\n".format(tl)

        ret += "App\n"
        ret += "-------------------------------------------------\n"

        for tl in [ln.strip() for ln in self.App.Dump().split("\n")]:
            ret += "    {0}\n".format(tl)

        ret += "Key\n"
        ret += "-------------------------------------------------\n"

        for tl in [ln.strip() for ln in self.Key.decode("ascii").split("\n")]:
            ret += "    {0}\n".format(tl)

        return ret


    @property
    def Terminals(self):
        return self._CfgTerminals


    @property
    def App(self):
        return self._CfgApp


    @property
    def Key(self):
        return self._Key


    @property
    def IsOk(self):
        return self._Ok
