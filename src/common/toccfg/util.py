import hashlib



class ConfigUtil(object):
    @classmethod
    def MakeNumeric(cls, text):
        ret = None
        value_str = unicode(text)

        try:
            ret = float(value_str)
        except ValueError:
            raise ValueError("Can not convert to number: {}".format(value_str))

        return ret


    @classmethod
    def MakeFloat(cls, text):
        return cls.MakeNumeric(text)


    @classmethod
    def MakeInt(cls, text):
        num = cls.MakeNumeric(text)
        return int(num)


    @classmethod
    def MakePosFloat(cls, text, allow_zero=False):
        ret = cls.MakeFloat(text)

        if ret < 0:
            raise ValueError("Can not convert to positive number: {}".format(text))

        if ret == 0 and not allow_zero:
            raise ValueError("Zero is not allowed: {}".format(text))

        return ret


    @classmethod
    def MakePosInt(cls, text, allow_zero=False):
        ret = cls.MakeInt(text)

        if ret < 0:
            raise ValueError("Can not convert to positive number: {}".format(text))

        if ret == 0 and not allow_zero:
            raise ValueError("Zero is not allowed: {}".format(text))

        return ret


    @classmethod
    def FileMD5Digest(self, filename):
        hash_md5 = hashlib.md5()
        with open(filename, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()
