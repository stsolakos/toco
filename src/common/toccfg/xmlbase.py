import xml.etree.ElementTree as ET
import re
import sys
import logging


LOG = logging.getLogger("")



class XmlConfig(object):
    def __init__(self, root=None):
        self._Root = root
        return


    @property
    def Root(self):
        return self._Root


    def ReadTags(self, parent, name):
        tags = parent.findall(name)

        if tags is None:
            return []

        return tags


    def ReadTag(self, parent, name, optional=False):
        tag = parent.find(name)

        if tag is None and optional == False:
            raise ValueError("{}: non-optional tag {} not found\n".format(parent.tag, name))

        return tag


    def ReadTagText(self, parent, name, default=None):
        optional = (default is not None)

        tag = self.ReadTag(parent, name, optional)

        if tag is None:
            if optional == True:
                return default
            else:
                raise ValueError("Tag {} is not optional".format(name))

        return tag.text


    def ReadTagAttribute(self, parent, name, attr, default=None):
        optional = (default is not None)

        tag = self.ReadTag(parent, name, optional)

        if tag is None:
            if optional == True:
                return default
            else:
                raise ValueError("Tag attribute {} is not optional for tag {}".format(attr, name))

        adict = tag.attrib

        try:
            ret = adict[attr]
        except KeyError:
            if default is None:
                raise ValueError("Tag attribute {} is not optional for tag {}".format(attr, name))
            else:
                ret = default

        return ret


    def Load(self, data):
        self._Root = ET.fromstring(data)
        return
