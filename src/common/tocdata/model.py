import os
import csv
import logging

from collections import OrderedDict

import simplekml
import gpxpy
import ezdxf
import pyproj
import xlsxwriter

from bs4 import BeautifulSoup


LOG = logging.getLogger("toco")



class TocoModel(object):
    def __init__(self):
        self._BaseName = None
        self._DirName = None
        self._PointsByName = OrderedDict()
        self._PointsByIndex = []
        return


    def __len__(self):
        return len(self.PointsByIndex)


    @property
    def IsOk(self):
        return (len(self.PointsByIndex) != 0)


    @property
    def BaseName(self):
        return self._BaseName


    @property
    def DirName(self):
        return self._DirName


    @property
    def PointsByName(self):
        return self._PointsByName


    @property
    def PointsByIndex(self):
        return self._PointsByIndex


    def Clear(self):
        self._BaseName = None
        self._DirName = None
        self._PointsByName = OrderedDict()
        self._PointsByIndex = []
        return


    def _CRSFromFilename(self, filename, default="epsg:4326"):
        ret = None

        try:
            name = os.path.basename(filename)
            ret = ":".join(name.split(".")[-3:-1])
        except:
            ret = default

        # Check whether this is a valid CRS
        try:
            test = pyproj.CRS.from_user_input(ret)
        except pyproj.exceptions.CRSError:
            LOG.exception("Exception while determining the coordinate system from filename {}. Falling back to epsg:4326:".format(filename))
            ret = "epsg:4326"

        return ret


    def LoadGpx(self, filename):
        self.Clear()

        try:
            with open(filename, "r", encoding='utf-8') as gpx_file:
                gpx = gpxpy.parse(gpx_file)

                # Load waypoints
                self._PointsByName = OrderedDict((waypoint.name, waypoint) for waypoint in gpx.waypoints)
                self._PointsByIndex = [self._PointsByName[k] for k in self._PointsByName]

                cindex = len(self._PointsByIndex)

                # Load tracks
                tindex = 0
                for track in gpx.tracks:
                    for segment in track.segments:
                        for point in segment.points:
                            waypoint = gpxpy.gpx.GPXWaypoint()
                            waypoint.name = 't{}_{}'.format(tindex, cindex)
                            waypoint.latitude = point.latitude
                            waypoint.longitude = point.longitude
                            waypoint.elevation = point.elevation
                            self._PointsByName[waypoint.name] = waypoint
                            self._PointsByIndex.append(waypoint)
                            cindex += 1

                    tindex += 1
        except:
            LOG.exception("Exception while loading GPX: {}".format(filename))
            self.Clear()

        self._DirName = os.path.dirname(filename)
        self._BaseName = os.path.splitext(os.path.basename(filename))[0]
        return


    def LoadCsv(self, filename, fmt=None):
        self.Clear()

        if fmt is None:
            fmt = self._CRSFromFilename(filename)

        with open(filename, newline="", encoding="utf-8-sig") as f:
            try:
                reader = csv.reader(f, delimiter=";", quoting=csv.QUOTE_NONE, dialect="excel")
                tr = pyproj.Transformer.from_crs(fmt, "epsg:4326", always_xy=True)

                for row in reader:
                    try:
                        waypoint = gpxpy.gpx.GPXWaypoint()
                        waypoint.name = str(row[0])
                        waypoint.longitude, waypoint.latitude = tr.transform(float(row[1].replace(",", ".")), float(row[2].replace(",", ".")))

                        try:
                            waypoint.elevation = float(row[3].replace(",", "."))
                        except:
                            pass

                        try:
                            waypoint.comment = row[4]
                        except:
                            pass
                    except:
                        continue

                    self._PointsByName[waypoint.name] = waypoint
                    self._PointsByIndex.append(waypoint)
            except:
                LOG.exception("Exception while loading CSV: {}".format(filename))
                self.Clear()
                return

        self._DirName = os.path.dirname(filename)
        self._BaseName = os.path.splitext(os.path.basename(filename))[0]
        return


    def LoadDxf(self, filename, fmt=None):
        self.Clear()

        if fmt is None:
            fmt = self._CRSFromFilename(filename)

        try:
            doc = ezdxf.readfile(filename)
            msp = doc.modelspace()
            tr = pyproj.Transformer.from_crs(fmt, "epsg:4326", skip_equivalent=True, always_xy=True)

            for e in msp.query("POINT"):
                waypoint = gpxpy.gpx.GPXWaypoint()
                waypoint.name = e.dxf.handle
                waypoint.longitude, waypoint.latitude = tr.transform(float(e.dxf.location.x), float(e.dxf.location.y))

                self._PointsByName[waypoint.name] = waypoint
                self._PointsByIndex.append(waypoint)

            for pl in msp.query("LWPOLYLINE"):
                for i in range(0, len(pl.lwpoints.values), 5):
                    p = len(self._PointsByIndex) + 1
                    waypoint = gpxpy.gpx.GPXWaypoint()
                    waypoint.name = "{}".format(p)
                    waypoint.longitude, waypoint.latitude = tr.transform(pl.lwpoints.values[i + 0], pl.lwpoints.values[i + 1])

                    self._PointsByName[waypoint.name] = waypoint
                    self._PointsByIndex.append(waypoint)
        except:
            LOG.exception("Exception while loading DXF: {}".format(filename))
            self.Clear()
            return

        self._DirName = os.path.dirname(filename)
        self._BaseName = os.path.splitext(os.path.basename(filename))[0]
        return


    def LoadKml(self, filename):
        self.Clear()

        try:
            with open(filename, "rt") as f:
                kml_doc = f.read()
                f.close()

            soup = BeautifulSoup(kml_doc, "lxml-xml")

            for tag_placemark in soup.find_all("Placemark"):
                tag_name = tag_placemark.find("name")

                if tag_name is None:
                    continue

                waypoint = gpxpy.gpx.GPXWaypoint()

                waypoint.name = tag_name.text

                tag_coordinates = tag_placemark.find("coordinates")

                if tag_coordinates is None:
                    continue

                fields = tag_coordinates.text.split(",")
                waypoint.longitude = float(fields[0])
                waypoint.latitude = float(fields[1])

                self._PointsByName[waypoint.name] = waypoint
                self._PointsByIndex.append(waypoint)
        except:
            LOG.exception("Exception while loading KML: {}".format(filename))
            self.Clear()
            return

        self._DirName = os.path.dirname(filename)
        self._BaseName = os.path.splitext(os.path.basename(filename))[0]
        return


    def SaveXlsx(self, filename):
        tr = pyproj.Transformer.from_crs("epsg:4326", "epsg:2100", always_xy=True)

        with xlsxwriter.Workbook(filename) as workbook:
            worksheet = workbook.add_worksheet()

            bold = workbook.add_format({"bold": True})
            text = workbook.add_format({"num_format": "@"})
            coord = workbook.add_format({"num_format": "#"})

            worksheet.write("A1", "Name", bold)
            worksheet.write("B1", "X", bold)
            worksheet.write("C1", "Y", bold)
            worksheet.write("D1", "X (text)", bold)
            worksheet.write("E1", "Y (text)", bold)
            worksheet.write("F1", "Elevation", bold)
            worksheet.write("G1", "Datetime", bold)
            row = 1

            for waypoint in self.PointsByIndex:
                lon, lat = tr.transform(waypoint.longitude, waypoint.latitude)
                worksheet.write(row, 0, waypoint.name)
                worksheet.write(row, 1, lon, coord)
                worksheet.write(row, 2, lat, coord)
                worksheet.write(row, 3, "{:.0f}".format(lon), text)
                worksheet.write(row, 4, "{:.0f}".format(lat), text)
                worksheet.write(row, 5, waypoint.elevation if waypoint.elevation is not None else "", text)
                worksheet.write(row, 6, waypoint.comment if waypoint.comment is not None else "", text)
                row += 1

        return


    def SaveKml(self, filename):
        kml = simplekml.Kml()

        for waypoint in self.PointsByIndex:
            kml.newpoint(name=waypoint.name, coords=[(waypoint.longitude, waypoint.latitude, waypoint.elevation)])  # lon, lat, optional height

        kml.save(filename)
        return
