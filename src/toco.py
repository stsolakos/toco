import datetime
import logging
import sys
import wx

import gui.log
from common.tocenv import TocoEnv

if TocoEnv.Get().Runtime.IsSource:
    # Additional source paths.
    # NOTE: Do not forget to add them to the spec, too!
    sys.path.insert(1, "../lib")

from gui.app import TocoApp


_ = wx.GetTranslation
LOG = logging.getLogger("")



def main():
    app = TocoApp()
    return app.MainLoop()



if __name__ == "__main__":
    try:
        main()
    except:
        LOG.exception("Exception when running main:")
