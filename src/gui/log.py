import sys
import logging
import logging.handlers

from common.tocenv import TocoEnv


# Initialize the loggers
stderr_handler = logging.StreamHandler(sys.stderr)
stderr_handler.setFormatter(logging.Formatter("%(asctime)s %(name)s %(levelname)s - %(message)s"))
stderr_handler.setLevel(logging.DEBUG)

full_file_handler = logging.handlers.RotatingFileHandler(TocoEnv.Get().Files.TocoLogFull, maxBytes=1024 * 1024, backupCount=7, encoding="utf-8")
full_file_handler.setFormatter(logging.Formatter("%(asctime)s %(name)-10.10s %(levelname)-5.5s - %(message)s"))
full_file_handler.setLevel(logging.INFO)

error_file_handler = logging.handlers.RotatingFileHandler(TocoEnv.Get().Files.TocoLogErr, maxBytes=1024 * 1024, backupCount=7, encoding="utf-8")
error_file_handler.setFormatter(logging.Formatter("%(asctime)s %(name)-10.10s %(levelname)-5.5s - %(message)s"))
error_file_handler.setLevel(logging.WARNING)

LOG = logging.getLogger("")
LOG.addHandler(full_file_handler)
LOG.addHandler(error_file_handler)

if TocoEnv.Get().Runtime.IsSource:
    LOG.addHandler(stderr_handler)

LOG.setLevel(logging.DEBUG)
