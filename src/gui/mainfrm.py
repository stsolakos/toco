import os
import logging

import pyproj
import wx
import wx.xrc

from .art.toco_ico import ART_TOCO_ICO


_ = wx.GetTranslation
LOG = logging.getLogger("gui")



class GpxDropTarget(wx.FileDropTarget):
    def __init__(self):
        wx.FileDropTarget.__init__(self)
        return


    def OnDropFiles(self, x, y, filenames):
        fname = filenames[-1]
        ext = os.path.splitext(fname)[1].lower()

        if ext in [".csv", ".txt"]:
            wx.GetApp().Model.LoadCsv(fname)
        elif ext in [".gpx"]:
            wx.GetApp().Model.LoadGpx(fname)
        elif ext in [".dxf"]:
            wx.GetApp().Model.LoadDxf(fname)
        elif ext in [".kml"]:
            wx.GetApp().Model.LoadKml(fname)
        else:
            return False

        wx.GetApp().FmMain.Reload()
        return True



class TocoFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self)

        # Creation
        wx.GetApp().XmlRes.LoadFrame(self, None, "ID_FM_MAIN")
        self.ChCoord = wx.xrc.XRCCTRL(self, "ID_CH_COORD")
        self.LcPoints = wx.xrc.XRCCTRL(self, "ID_LC_POINTS")
        self.BtExportXlsx = wx.xrc.XRCCTRL(self, "ID_BT_EXPORT_XLSX")
        self.BtExportKml = wx.xrc.XRCCTRL(self, "ID_BT_EXPORT_KML")
        self.BtExit = wx.xrc.XRCCTRL(self, "ID_BT_EXIT")

        # Initialization
        self.SetIcon(ART_TOCO_ICO.GetIcon())
        self.LcPoints.SetDropTarget(GpxDropTarget())

        # Events
        self.Bind(wx.EVT_CLOSE, self.onClose)

        self.Bind(wx.EVT_CHOICE, self.onChCoord, id=wx.xrc.XRCID("ID_CH_COORD"))
        self.Bind(wx.EVT_BUTTON, self.onBtExportXlsx, id=wx.xrc.XRCID("ID_BT_EXPORT_XLSX"))
        self.Bind(wx.EVT_BUTTON, self.onBtExportKml, id=wx.xrc.XRCID("ID_BT_EXPORT_KML"))
        self.Bind(wx.EVT_BUTTON, self.onBtExit, id=wx.xrc.XRCID("ID_BT_EXIT"))

        self.Bind(wx.EVT_UPDATE_UI, self.onUpdateButtons, id=wx.xrc.XRCID("ID_BT_EXPORT_XLSX"), id2=wx.xrc.XRCID("ID_BT_EXIT"))

        LOG.info("Created the main frame.")
        return


    def onClose(self, event):
        event.Skip()
        return


    def onChCoord(self, event):
        self.LcPoints.Converter = pyproj.Transformer.from_crs("epsg:4326", self.ChCoord.GetStringSelection(), always_xy=True)
        return


    def onBtExportXlsx(self, event):
        dlg = wx.FileDialog(self, message="Export XLSX file as ...", defaultDir=wx.GetApp().Model.DirName, defaultFile=wx.GetApp().Model.BaseName, wildcard="*.xlsx", style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        if dlg.ShowModal() == wx.ID_OK:
            wx.GetApp().Model.SaveXlsx(dlg.GetPath())

        return


    def onBtExportKml(self, event):
        dlg = wx.FileDialog(self, message="Export KML file as ...", defaultDir=wx.GetApp().Model.DirName, defaultFile=wx.GetApp().Model.BaseName, wildcard="*.kml", style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        if dlg.ShowModal() == wx.ID_OK:
            wx.GetApp().Model.SaveKml(dlg.GetPath())

        return


    def onBtExit(self, event):
        self.Close()
        return


    def onUpdateButtons(self, event):
        if event.GetId() in [self.BtExportXlsx.GetId(), self.BtExportKml.GetId()]:
            event.Enable(wx.GetApp().Model.IsOk)

        return


    def Reload(self):
        self.LcPoints.SetItemCount(len(wx.GetApp().Model.PointsByIndex))
        self.LcPoints.Refresh()
        return
