import os
import os
import logging

import wx
import wx.xrc

from common.tocenv import TocoEnv

if TocoEnv.Get().Runtime.IsWindows:
    from toclib.namedmutex import NamedMutex

import gui.guires
from gui.mainfrm import TocoFrame
from common.tocdata.model import TocoModel

# Import modules required by XRC, pickle etc here so that
# the PyInstaller will freeze them as well.
import gui.pointlist


_ = wx.GetTranslation
LOG = logging.getLogger("toco")



class TocoApp(wx.App):
    def OnInit(self):
        self._XmlRes = None
        no_wx_log = wx.LogNull()
        self.SetAppName("toco")
        self.Checker = wx.SingleInstanceChecker(self.GetAppName())

        if self.Checker.IsAnotherRunning():
            LOG.error("Another instance of the application is running. Will exit now...")
            return False

        del no_wx_log

        if TocoEnv.Get().Runtime.IsWindows:
            mutex_setup = NamedMutex(b"toco_running_mutex")

        LOG.info("Starting toco...")

        if not self.initResources():
            wx.MessageBox(_("Could not load resources!"), _("Error"), wx.ICON_ERROR)
            return False

        self._Model = TocoModel()
        self.FmMain = TocoFrame()

        self.SetExitOnFrameDelete(True)
        self.SetTopWindow(self.FmMain)
        self.FmMain.Show(True)
        return True


    @property
    def Model(self):
        return self._Model


    @property
    def XmlRes(self):
        if self._XmlRes is None:
            self._XmlRes = wx.xrc.XmlResource()

        return self._XmlRes


    def initResources(self):
        self.XmlRes.InitAllHandlers()
        self.XmlRes.InsertHandler(gui.guires.ShellXmlHandler())

        try:
            import gui.setup_resources as setup_resources
            ret = wx.GetApp().XmlRes.LoadFromBuffer(setup_resources.TOCO_RESOURCE_STR.encode("utf-8"))
        except ImportError:
            ret = False

        if not ret:
            ret = self.initExtResources()

        return ret


    def initExtResources(self):
        if not self.XmlRes.Load("../rsrc/toco.xrc") and not self.XmlRes.Load("./toco.xrc"):
            return False

        return True
