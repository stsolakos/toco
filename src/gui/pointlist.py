import sys
import datetime
import logging

import pyproj
import wx
import wx.xrc


_ = wx.GetTranslation
LOG = logging.getLogger("gui")



class PointList(wx.ListCtrl):
    def __init__(self):
        wx.ListCtrl.__init__(self)

        # Creation

        # Initialization
        self._Converter = None

        # Events
        self.Bind(wx.EVT_WINDOW_CREATE, self.onCreate)
        self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.onContext)
        return


    @property
    def Converter(self):
        return self._Converter


    @Converter.setter
    def Converter(self, conv):
        self._Converter = conv
        self.Refresh(True)
        return


    def CreateContextMenu(self):
        ret = wx.GetApp().XmlRes.LoadMenu("ID_MN_TL_CONTEXT")
        return ret


    def onCreate(self, event):
        if self is event.GetEventObject():
            self.InsertColumn(0, _("Name"))
            self.InsertColumn(1, _("X"))
            self.InsertColumn(2, _("Y"))
            self.InsertColumn(3, _("Elevation"))
            self.InsertColumn(4, _("Comment"))
            self.SetColumnWidth(0, 50)
            self.SetColumnWidth(1, 140)
            self.SetColumnWidth(2, 140)
            self.SetColumnWidth(3, 60)
            self.SetColumnWidth(4, 200)

        event.Skip()
        return


    def onContext(self, event):
        return


    def OnGetItemText(self, item, col):
        model = wx.GetApp().Model
        p = model.PointsByIndex[item]

        if col == 0:
            return str(p.name)

        elif col == 1:
            lon, lat = self._Converter.transform(p.longitude, p.latitude) if self._Converter is not None else (p.longitude, p.latitude)
            return str(lon)

        elif col == 2:
            lon, lat = self._Converter.transform(p.longitude, p.latitude) if self._Converter is not None else (p.longitude, p.latitude)
            return str(lat)

        elif col == 3:
            return str(p.elevation) if p.elevation is not None else ""

        elif col == 4:
            return str(p.comment) if p.comment is not None else ""

        return "Item %d, column %d" % (item, col)


    def OnGetItemAttr(self, item):
        return None
