import wx
import wx.xrc
import wx.py.shell



class ShellXmlHandler(wx.xrc.XmlResourceHandler):
    def __init__(self):
        wx.xrc.XmlResourceHandler.__init__(self)
        self.AddWindowStyles()
        return


    def CanHandle(self, node):
        return self.IsOfClass(node, "Shell")


    def DoCreateResource(self):
        assert self.GetInstance() is None

        shell = wx.py.shell.Shell(self.GetParentAsWindow(),
                              self.GetID(),
                              self.GetPosition(),
                              self.GetSize(),
                              self.GetStyle()
                              )

        self.SetupWindow(shell)
        self.CreateChildren(shell)

        return shell

